import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { SigninComponent } from './signin.component';
import { ApiService } from '../api.service';

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninComponent ],
      providers : [ApiService],
      imports : [HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('No deberia de existir una sesion',()=>{
    expect(localStorage.getItem('user')).toEqual(null);
  });

  it('Comprobar contrasenia',()=>{
    let passEqual = component.verificarPass('abcdef123','abcdef123');
    expect(passEqual).toBeTrue();

    passEqual = component.verificarPass('12345afsd','12345afsa');
    expect(passEqual).toBeFalse();

    passEqual = component.verificarPass('12345678','12345678');
    expect(passEqual).toBeFalse();

    passEqual = component.verificarPass('abcd123','abcd123');
    expect(passEqual).toBeFalse();
  });

  it('Comprobar campos vacios',()=>{
    let newUsr = component.newUsr;
    expect(newUsr.username).toEqual('');
    expect(newUsr.password).toEqual('');
    expect(newUsr.fullname).toEqual('');
    expect(newUsr.email).toEqual('');
    expect(newUsr.confirmPass).toEqual('');
  });

  it('Recibir Estado del servidodr',()=>{
    inject([ApiService],async(rest:ApiService)=>{
      let result;

      let user = component.createUser('Test','erflod5','Test','Test');
      rest.signin(user).subscribe((data)=>{
        expect(data.status).toBeDefined();
      },
      (err)=>{

      })
    })
  });
});
