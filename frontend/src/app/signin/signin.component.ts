import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  public newUsr = {
    fullname : '',
    username : '',
    email : '',
    password : '',
    confirmPass : ''
  }

  constructor(private apiService : ApiService) { }

  ngOnInit(): void {
  }

  fullname(event: any){
    this.newUsr.fullname = event.target.value;
  }

  user(event: any) {
    this.newUsr.username = event.target.value;
  }

  email(event: any){
    this.newUsr.email = event.target.value;
  }

  //Handler Password
  pass(event: any) {
    this.newUsr.password = event.target.value;
  }

  //Handler ConfirmPassword
  confirmPass(event: any){
    this.newUsr.confirmPass = event.target.value;
  }

  signin(){
    console.log(this.newUsr);
    //TODO comprobar pass 8 caracteres y alfa
    if(this.verificarPass(this.newUsr.password,this.newUsr.confirmPass)){
      this.apiService.register(this.newUsr).subscribe(
        (res)=>{
          console.log(res);
          if(res.status){
            alert("Usuario creado");
            //Todo redirigir a pantalla principal
          }
          else{
            alert('Ya existe el usuario');
          }
        },
        (err)=>{
          console.log(err);
        }
      );
    }
    else{
      alert("Las contrasenas no coindicen");
    }
  }

  verificarPass(password: string, confirmPass : string){
    if(password != confirmPass){
      alert('Las contrasenias no coinciden');
      return false;
    }

    if(password.length < 8){
      alert('La contrasenia debe tener al menos 8 caracteres');
      return false;
    }


    let hasNumber = /\d/.test(password);
    let hasUpper = /[A-Z]/.test(password);
    let hasLower = /[a-z]/.test(password);
    if(!(hasNumber && (hasUpper || hasLower))){
      alert('La contrasenia no cuenta con numeros y digitos');
      return false;
    }
    return true;
  }

  createUser(fullname,username,email,password){
    let usr = {
      fullname,
      username,
      email,
      password
    }
    return usr;
  }
}
