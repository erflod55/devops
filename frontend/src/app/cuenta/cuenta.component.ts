import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {

  public currentUser;
  constructor(private apiService: ApiService, private router: Router) { 
    this.currentUser = this.apiService.currentUser();
  }

  ngOnInit(): void {
    this.apiService.cuenta(this.currentUser).subscribe(
      (data) =>{
        console.log(data);
        this.currentUser = data.user[0];
      },
      (err) =>{
        console.error(err);
      }
    )
  }

  logout(){
    this.apiService.removeUser();
    this.router.navigate(['/login']);
  }
}
