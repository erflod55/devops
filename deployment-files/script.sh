  #!/bin/sh
kubectl create -f ./secrets.yml
kubectl create -f ./mysql-compose.yml
kubectl create -f ./mysql-service.yml
kubectl create -f ./node-compose.yml
kubectl create -f ./node-service.yml
kubectl get services
minikube service spring-service --url