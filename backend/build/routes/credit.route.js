"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const credit_controller_1 = require("../controller/credit.controller");
class CreditRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/', credit_controller_1.creditController.create);
        this.router.get('/', credit_controller_1.creditController.get);
        this.router.get('/pendientesHistorial', credit_controller_1.creditController.pendientesHistorial);
        this.router.get('/aprobadosHistorial', credit_controller_1.creditController.aprobadosHistorial);
        this.router.get('/pagadosHistorial', credit_controller_1.creditController.pagadosHistorial);
        this.router.get('/aprobados', credit_controller_1.creditController.aprobados);
        this.router.get('/pendientes', credit_controller_1.creditController.pendientes);
        this.router.put('/aprobarCredito', credit_controller_1.creditController.aprobarCredito);
        this.router.get('/cerrados', credit_controller_1.creditController.cerrados);
        this.router.put('/pagarCredito', credit_controller_1.creditController.pagarCredito);
    }
}
const creditRoute = new CreditRoute();
exports.default = creditRoute.router;
