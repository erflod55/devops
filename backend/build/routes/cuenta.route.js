"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cuenta_controller_1 = require("../controller/cuenta.controller");
class UserRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/', cuenta_controller_1.cuentaController.consultaSaldo);
        this.router.post('/transferencia', cuenta_controller_1.cuentaController.transferencia);
        this.router.get('/pagosHistorial', cuenta_controller_1.cuentaController.pagosHistorial);
        this.router.get('/cobrosHistorial', cuenta_controller_1.cuentaController.cobrosHistorial);
    }
}
const userRoute = new UserRoute();
exports.default = userRoute.router;
