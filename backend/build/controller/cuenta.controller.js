"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../database/connection");
class CuentaController {
    constructor() {
    }
    consultaSaldo(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { username, cuenta } = req.body;
            let user = [cuenta, username];
            connection_1.pool.query('SELECT name,username,email,saldo,cuenta,idusuario FROM USUARIO WHERE cuenta = ? AND username = ? ', user, (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    res.send({ status: true, user: data });
                }
            });
        });
    }
    transferencia(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { origen, destino, saldo, iduser } = req.body;
            connection_1.pool.query('SELECT * FROM USUARIO WHERE cuenta = ? AND idusuario = ?', [origen, iduser], (err, data) => {
                let user = data[0];
                if (user == null) {
                    return res.send({ status: false, message: 'No existe la cuenta origen' });
                }
                if (user.saldo < saldo) {
                    return res.send({ status: false, message: 'No posee fondos' });
                }
                connection_1.pool.query('SELECT * FROM USUARIO WHERE cuenta = ?', [destino], (err, data) => {
                    let userDestino = data[0];
                    if (userDestino == null) {
                        return res.send({ status: false, message: 'No existe la cuenta destino' });
                    }
                    connection_1.pool.query('CALL transferencia(?,?,?)', [iduser, userDestino.idusuario, saldo], (err, data) => {
                        console.log(data);
                        return res.send({ status: true, data: data[0][0] });
                    });
                });
            });
        });
    }
    pagosHistorial(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { cuenta } = req.query;
            let sql = 'CALL pagosHistorial(?)';
            connection_1.pool.query(sql, [cuenta], (err, data) => {
                res.send(data[0]);
            });
        });
    }
    cobrosHistorial(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { cuenta } = req.query;
            let sql = 'CALL cobrosHistorial(?)';
            connection_1.pool.query(sql, [cuenta], (err, data) => {
                res.send(data[0]);
            });
        });
    }
}
exports.cuentaController = new CuentaController();
/**
 *
 *
 *
 */ 
