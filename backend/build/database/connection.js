"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mysql2_1 = require("mysql2");
exports.pool = mysql2_1.createPool({
    host: process.env.HOST || 'localhost',
    user: process.env.USER || 'root',
    password: process.env.PASSWORD || 'secret',
    database: process.env.DB || 'ANALISIS',
    port: Number(process.env.PORT) || 3307
});
