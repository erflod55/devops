import {Router} from 'express';
import {userController} from '../controller/user.controller';

class UserRoute {
    router : Router = Router();

    constructor(){
        this.config();
    }

    config():void{
        this.router.post('/',userController.create);
        this.router.get('/',userController.get);
    }
}

const userRoute = new UserRoute();
export default userRoute.router;