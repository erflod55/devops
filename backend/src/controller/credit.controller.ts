import {Request, Response} from 'express';
import {pool} from '../database/connection';
import {sendMail} from './mail.controller';

class CreditController{
    constructor(){
    }

    public async create(req: Request, res: Response){
        let {monto, descripcion, idUsuario} = req.body;
        let fechaPedido = new Date();
        let credit = [monto,descripcion,fechaPedido,false,idUsuario];
        console.log(credit);
        pool.query('INSERT INTO CREDITO(monto,descripcion,fechapedido,estado,USUARIO_idusuario) values(?,?,?,?,?)',credit,(err:any,data:any)=>{
            if(err){
                res.send({status : false});
            }else{
                console.log(data);
                res.send({status:true});
            }
        });
    }

    public async get(req: Request, res: Response){
        let {idUsuario} = req.query;
        let credit = [idUsuario];
        pool.query('SELECT * FROM CREDITO WHERE USUARIO_idusuario = ?',credit,(err:any,data:any)=>{
            if(err){
                res.send({status : false});
            }else{
                res.send({status:true, data:data});
            }
        });
    }

    public async aprobadosHistorial(req: Request, res: Response){
        let {cuenta} = req.query;
        let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 1';
        pool.query(sql,[cuenta],(err: any, data:any)=>{
            res.send(data);
        })
    }

    public async pendientesHistorial(req: Request, res:  Response){
        let {cuenta} = req.query;
        let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 0';
        pool.query(sql,[cuenta],(err: any, data:any)=>{
            res.send(data);
        })
    }

    public async pagadosHistorial(req: Request, res:  Response){
        let {cuenta} = req.query;
        let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 2';
        pool.query(sql,[cuenta],(err: any, data:any)=>{
            res.send(data);
        })
    }

    public async aprobados(req: Request, res: Response){
        let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM CREDITO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 1';
        pool.query(sql,(err: any, data:any)=>{
            if(err){
                console.log(err);
            }
            else res.send(data);
        })
        console.log('works');
    }

    public async pendientes(req: Request, res:  Response){
        let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM USUARIO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 0';
        pool.query(sql,(err: any, data:any)=>{
            res.send(data);
        })
    }

    public async cerrados(req: Request, res:  Response){
        let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM USUARIO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 2';
        pool.query(sql,(err: any, data:any)=>{
            res.send(data);
        })
    }

    public async aprobarCredito(req: Request, res: Response){
        let {idcredito} = req.body;
        let fechaaproved = new Date();
        let fechafinal = new Date();
        fechafinal.setDate(fechafinal.getDate() + 30);
        pool.query('CALL aprobarCredito(?,?,?)',[fechaaproved,fechafinal,idcredito],(err:any,data:any)=>{
            if(err){
                res.send({status : false});
            }else{
                console.log(data[0][0]);
                sendMail('appmorpheus5@gmail.com',data[0][0].email,'Credito aprobado','Su credio ha sido aprobado');
                res.send({status : true, data:data});
            }
        });
    }

    public async pagarCredito(req: Request, res: Response){
        let {idcredito} = req.body;
        pool.query('CALL pagarCredito(?)',[idcredito],(err:any,data:any)=>{
            if(err){
                res.send({status:false});
            }else{
                console.log(data);
                if(data[0][0].estado == 1)
                {
                    res.send({status:true});
                }
                else
                {
                    res.send({status:false});
                }
            }
        });
    }

}

export const creditController = new CreditController();