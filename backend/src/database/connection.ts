import { createPool } from 'mysql2';

export const pool = createPool({
    host : process.env.HOST || 'localhost',
    user : process.env.USER || 'root',
    password : process.env.PASSWORD || 'secret',
    database : process.env.DB || 'ANALISIS',
    port : Number(process.env.PORT) || 3307 
});
