//Libs
import express from 'express';
import cors from 'cors';
import userRoute from './routes/user,route';
import cuentaRoute from './routes/cuenta.route';
import creditRoute from './routes/credit.route';
import {pool} from './database/connection';

//Server
const app = express();

//Conf
app.set('port',process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({extended : false}));
app.use(cors());

//Routes
app.use('/api/User',userRoute);
app.use('/api/cuenta',cuentaRoute);
app.use('/api/credit',creditRoute);
app.get('/',(req, res)=>{
    res.send('Hello world desde K8S');
});

//Server
app.listen(app.get('port'),()=>{
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`);
    try {
        pool.query('SELECT curdate() from dual',(err:any,data:any)=>{
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
    } catch (error) {
        console.log(error);
    }
});